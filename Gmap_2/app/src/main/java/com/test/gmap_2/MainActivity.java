package com.test.gmap_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;


public class MainActivity extends AppCompatActivity {

    private ImageView icon;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.icon = (ImageView) findViewById(R.id.my_icon);

        icon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent nextActivity = new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(nextActivity);
                finish();
            }
        });
    }
}